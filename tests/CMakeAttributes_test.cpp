#include <../app/cmakeattributes.cpp>

#include <gmock/gmock.h>

using std::vector;
using std::string;

TEST(CMakeAttributes, CMake_version)
{
    const string default_version { "1.0" };

    CMakeAttributes attr;
    ASSERT_EQ(attr.cmake_version(), default_version);

    const string ver4 { "4.0" };
    attr.cmake_version(ver4);
    ASSERT_EQ(attr.cmake_version(), ver4);

    attr.cmake_version("");
    ASSERT_EQ(attr.cmake_version(), default_version);

    attr.cmake_version("   ");
    ASSERT_EQ(attr.cmake_version(), default_version);
}

TEST(CMakeAttributes, Project_name)
{
    const string default_name { "Default" };

    CMakeAttributes attr;
    ASSERT_EQ(attr.project_name(), default_name);

    const string project_name { "Example" };
    const string expected { "Example" };
    attr.project_name(project_name);
    ASSERT_EQ(attr.project_name(), expected);

    const string wrong_name { "   .)  " };
    attr.project_name(wrong_name);
    ASSERT_EQ(attr.project_name(), default_name);

}

TEST(CMakeAttributes, Compiler_flags)
{
    CMakeAttributes attr;

    ASSERT_TRUE(attr.compiler_flags().empty());

    const string flags { "-Wall -Wextra" };
    attr.compiler_flags(flags);

    const vector<string> expected {"-Wall", "-Wextra"};

    ASSERT_EQ(attr.compiler_flags(), expected);
}

TEST(CMakeAttributes, Sources_mask)
{
    CMakeAttributes attr;

    ASSERT_TRUE(attr.masks_sources().size() == 1);

    attr.masks_sources("");
    ASSERT_TRUE(attr.masks_sources().empty());

    const string s_masks { "cpp c" };
    attr.masks_sources(s_masks);

    const vector<string> expected { "cpp", "c"};

    ASSERT_EQ(attr.masks_sources(), expected);
}

TEST(CMakeAttributes, Headers_mask)
{
    CMakeAttributes attr;

    ASSERT_TRUE(attr.masks_headers().size() == 1);

    const string h_masks { "hpp h" };
    attr.masks_headers(h_masks);

    const vector<string> expected { "hpp", "h"};

    ASSERT_EQ(attr.masks_headers(), expected);
}

TEST(CMakeAttributes, Sanitizers)
{
    CMakeAttributes attr;

    ASSERT_TRUE(attr.sanitizers().empty());

    const vector<Sanitizers> sanitizers {
        Sanitizers::address, Sanitizers::memory,
        Sanitizers::undefined, Sanitizers::integer_overflow
    };

    attr.sanitizers(sanitizers);

    const vector<string> expected {
        "-fsanitize=address", "-fsanitize=leak",
        "-fsanitize=undefined", "-fsanitize=signed-integer-overflow"
    };

    ASSERT_EQ(attr.sanitizers(), expected);
}

TEST(CMakeAttributes, Benchmark)
{
    CMakeAttributes attr;
    ASSERT_FALSE(attr.benchmark());

    attr.benchmark(true);
    ASSERT_TRUE(attr.benchmark());
}
