//#include <../app/cmakelists.cpp>
#include <../app/cmakelists.cpp>
#include <../app/cmakeattributes.h>

#include <gmock/gmock.h>

using std::vector;
using std::string;

TEST(CMakeLists, CMake_version)
{
    CMakeAttributes attr;

    attr.cmake_version("2.5");

    const string expected
        {
            "cmake_minimum_required(VERSION 2.5)\n"
            "project(Default)\n"
            R"(file(GLOB CPPS "*.cpp"))" "\n"
            R"(file(GLOB HPPS "*.h"))"   "\n"
            "add_executable(${PROJECT_NAME} ${CPPS} ${HPPS})\n"
        };

    const auto result = CMakeLists::generateCMakeListsFile(attr);
    EXPECT_EQ(result, expected);
}

TEST(CMakeLists, Project_name)
{
    CMakeAttributes attr;
    attr.project_name("Lite");

    const string expected
        {
            "cmake_minimum_required(VERSION 1.0)\n"
            "project(Lite)\n"
            R"(file(GLOB CPPS "*.cpp"))" "\n"
            R"(file(GLOB HPPS "*.h"))"   "\n"
            "add_executable(${PROJECT_NAME} ${CPPS} ${HPPS})\n"
        };

    const auto result = CMakeLists::generateCMakeListsFile(attr);
    EXPECT_EQ(result, expected);
}

TEST(CMakeLists, Compiler_flags)
{
    CMakeAttributes attr;
    attr.compiler_flags("-Wall -Wextra");

    const string expected
        {
            "cmake_minimum_required(VERSION 1.0)\n"
            "project(Default)\n"
            R"(set(CMAKE_CXX_FLAGS " -Wall -Wextra"))" "\n"
            R"(file(GLOB CPPS "*.cpp"))" "\n"
            R"(file(GLOB HPPS "*.h"))"   "\n"
            "add_executable(${PROJECT_NAME} ${CPPS} ${HPPS})\n"
        };

    const auto result = CMakeLists::generateCMakeListsFile(attr);
    EXPECT_EQ(result, expected);
}

TEST(CMakeLists, Sanitizers)
{
    CMakeAttributes attr;
    attr.sanitizers(vector<Sanitizers>{ Sanitizers::address, Sanitizers::memory });

    const string expected
        {
            "cmake_minimum_required(VERSION 1.0)\n"
            "project(Default)\n"
            R"(set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=address -fsanitize=leak"))" "\n"
            R"(file(GLOB CPPS "*.cpp"))" "\n"
            R"(file(GLOB HPPS "*.h"))"   "\n"
            "add_executable(${PROJECT_NAME} ${CPPS} ${HPPS})\n"
        };
    const auto result = CMakeLists::generateCMakeListsFile(attr);
    EXPECT_EQ(result, expected);
}

TEST(CMakeLists, Masks_sources)
{
    CMakeAttributes attr;
    attr.masks_sources("cpp c cxx");

    const string expected
        {
            "cmake_minimum_required(VERSION 1.0)\n"
            "project(Default)\n"
            R"(file(GLOB CPPS "*.cpp" "*.c" "*.cxx"))" "\n"
            R"(file(GLOB HPPS "*.h"))"   "\n"
            "add_executable(${PROJECT_NAME} ${CPPS} ${HPPS})\n"
        };

    const auto result = CMakeLists::generateCMakeListsFile(attr);
    EXPECT_EQ(result, expected);
}

TEST(CMakeLists, Masks_headers)
{
    CMakeAttributes attr;
    attr.masks_headers("h hpp");

    const string expected
        {
            "cmake_minimum_required(VERSION 1.0)\n"
            "project(Default)\n"
            R"(file(GLOB CPPS "*.cpp"))" "\n"
            R"(file(GLOB HPPS "*.h" "*.hpp"))"   "\n"
            "add_executable(${PROJECT_NAME} ${CPPS} ${HPPS})\n"
        };

    const auto result = CMakeLists::generateCMakeListsFile(attr);
    EXPECT_EQ(result, expected);
}

TEST(CMakeLists, GenericTest)
{
    CMakeAttributes attr;
    attr.project_name("New_project");
    attr.cmake_version("4.0");
    attr.sanitizers(std::vector<Sanitizers>{Sanitizers::undefined, Sanitizers::memory});
    attr.masks_headers("h hpp");
    attr.masks_sources("c cpp");
    attr.compiler_flags("-Wall -Wextra -std=c++17");
    attr.benchmark(true);

    const string expected
        {
            "cmake_minimum_required(VERSION 4.0)\n"
            "project(New_project)\n"
            R"(set(CMAKE_CXX_FLAGS " -Wall -Wextra -std=c++17"))" "\n"
            R"(set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=undefined -fsanitize=leak"))" "\n"
            R"(file(GLOB CPPS "*.c" "*.cpp"))" "\n"
            R"(file(GLOB HPPS "*.h" "*.hpp"))"   "\n"
            "add_executable(${PROJECT_NAME} ${CPPS} ${HPPS})\n"
            "target_link_libraries(${PROJECT_NAME} PUBLIC benchmark benchmark_main pthread)\n"
        };

    const auto result = CMakeLists::generateCMakeListsFile(attr);
    EXPECT_EQ(result, expected);
}
