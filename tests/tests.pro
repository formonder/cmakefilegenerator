TEMPLATE = app

QT += testlib

CONFIG -= debug_and_release
CONFIG += testcase
CONFIG += console

QMAKE_CXXFLAGS += -std=c++17 -lstdc++fs

TARGET = Tests
INCLUDEPATH += ../GoogleTest/include

LIBS += -L../GoogleTest/lib -lGoogleTest

include(../GoogleTest/GoogleTest.pri)

SOURCES += \
        CMakeAttributes_test.cpp \
        CMakeLists_test.cpp \
        main.cpp
