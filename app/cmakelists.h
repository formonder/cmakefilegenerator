#ifndef CMAKELISTS_H
#define CMAKELISTS_H

#include <string>
#include <vector>

enum class Sanitizers;
class CMakeAttributes;

class CMakeLists
{
public:
    static void save_file(const std::string& path, const std::string& cmake_lists);
    static std::string generateCMakeListsFile(const CMakeAttributes& attributes);
private:
    static std::string add_version(const std::string& version);
    static std::string add_project_name(const std::string& project_name);
    static std::string add_compiler_flags(const std::vector<std::string>& compiler_flags);
    static std::string add_sanitizers(const std::vector<std::string>& sanitizers);
    static std::string add_masks_sources(const std::vector<std::string>& masks);
    static std::string add_masks_headers(const std::vector<std::string>& masks);
    static std::string add_executable();
    static std::string link_google_benchmark(bool is_enabled);
};

#endif // CMAKELISTS_H
