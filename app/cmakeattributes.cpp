#include "cmakeattributes.h"

#include <algorithm>
#include <iterator>
#include <sstream>

namespace
{
static const std::string default_cmake_version { "1.0" };
static const std::string default_project_name { "Default" };
}

CMakeAttributes::CMakeAttributes()
    : cmake_version_ { default_cmake_version }
    , project_name_ { default_project_name }
    , masks_sources_ { "cpp" }
    , masks_headers_ { "h" }
    , benchmark_ { false }
{

}

const std::string& CMakeAttributes::project_name() const
{
    return project_name_;
}

void CMakeAttributes::project_name(const std::string &name)
{
    if(std::any_of(begin(name), end(name), [](unsigned char c) { return std::isalnum(c);}))
    {
        project_name_ = name;
    }
    else
    {
        project_name_ = default_project_name;
    }
}

const std::string& CMakeAttributes::cmake_version() const
{
    return cmake_version_;
}

void CMakeAttributes::cmake_version(const std::string& version)
{
    if(std::any_of(begin(version), end(version), [](unsigned char c) { return std::isdigit(c); }))
    {
        cmake_version_ = version;
    }
    else
    {
        cmake_version_ = default_cmake_version;
    }
}

std::vector<std::string> split(const std::string& line)
{
    std::vector<std::string> tokens;

    std::stringstream ss(line);

    std::copy(
        std::istream_iterator<std::string>(ss),
        std::istream_iterator<std::string>{},
        back_inserter(tokens));

    return tokens;
}

const std::vector<std::string> &CMakeAttributes::compiler_flags() const
{
    return compiler_flags_;
}

void CMakeAttributes::compiler_flags(const std::string &flags)
{
    compiler_flags_ = split(flags);
}

const std::vector<std::string> &CMakeAttributes::masks_sources() const
{
    return masks_sources_;
}

void CMakeAttributes::masks_sources(const std::string &masks)
{
    masks_sources_ = split(masks);
}

const std::vector<std::string> &CMakeAttributes::masks_headers() const
{
    return masks_headers_;
}

void CMakeAttributes::masks_headers(const std::string &masks)
{
    masks_headers_ = split(masks);
}

const std::vector<std::string> &CMakeAttributes::sanitizers() const
{
    return sanitizers_;
}

std::string sanitizer_flag(const Sanitizers& sanitizer)
{
    switch (sanitizer)
    {
    case Sanitizers::address:          return "-fsanitize=address";
    case Sanitizers::integer_overflow: return "-fsanitize=signed-integer-overflow";
    case Sanitizers::memory:           return "-fsanitize=leak";
    case Sanitizers::undefined:        return "-fsanitize=undefined";
    default: return {};
    }
}

void CMakeAttributes::sanitizers(const std::vector<Sanitizers> &masks)
{
    for(const auto& mask : masks)
    {
        sanitizers_.push_back(sanitizer_flag(mask));
    }
}

bool CMakeAttributes::benchmark() const
{
    return benchmark_;
}

void CMakeAttributes::benchmark(bool is_enabled)
{
    benchmark_ = is_enabled;
}
