#include "cmakelists.h"
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>

#include "cmakeattributes.h"

namespace fs = std::filesystem;

std::string CMakeLists::generateCMakeListsFile(const CMakeAttributes &attributes)
{
    std::string CMakeListsTxt;

    CMakeListsTxt += add_version(attributes.cmake_version());
    CMakeListsTxt += add_project_name(attributes.project_name());
    CMakeListsTxt += add_compiler_flags(attributes.compiler_flags());
    CMakeListsTxt += add_sanitizers(attributes.sanitizers());
    CMakeListsTxt += add_masks_sources(attributes.masks_sources());
    CMakeListsTxt += add_masks_headers(attributes.masks_headers());
    CMakeListsTxt += add_executable();
    CMakeListsTxt += link_google_benchmark(attributes.benchmark());

    return CMakeListsTxt;
}

std::string CMakeLists::add_version(const std::string &version)
{
    return "cmake_minimum_required(VERSION " + version + ")\n";
}

std::string CMakeLists::add_project_name(const std::string &project_name)
{
    return "project(" + project_name + ")\n";
}

std::string CMakeLists::add_compiler_flags(const std::vector<std::string> &compiler_flags)
{
    if(compiler_flags.empty())
    {
        return {};
    }

    std::string all_compiler_flags {"set(CMAKE_CXX_FLAGS \""};

    for(const auto& compiler_flag : compiler_flags)
    {
        const std::string space { " " };
        all_compiler_flags += space + compiler_flag;
    }

    all_compiler_flags += "\")\n";

    return all_compiler_flags;
}

std::string CMakeLists::add_sanitizers(const std::vector<std::string> &sanitizers)
{
    if(sanitizers.empty())
    {
        return {};
    }

    std::string all_sanitizers {R"(set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS})"};

    for(const auto& sanitizer : sanitizers)
    {
        const std::string space { " " };
        all_sanitizers += space + sanitizer;
    }

    all_sanitizers += "\")\n";

    return all_sanitizers;
}

std::string CMakeLists::add_masks_sources(const std::vector<std::string> &masks)
{
    std::string source_masks {"file(GLOB CPPS"};

    for(const auto& mask : masks)
    {
        const std::string space { " " };

        source_masks += space + "\"*." + mask + "\"";
    }

    source_masks += ")\n";

    return source_masks;
}

std::string CMakeLists::add_masks_headers(const std::vector<std::string> &masks)
{
    std::string headers_masks {"file(GLOB HPPS"};

    for(const auto& mask : masks)
    {
        const std::string space { " " };

        headers_masks += space + "\"*." + mask + "\"";
    }

    headers_masks += ")\n";

    return headers_masks;
}

std::string CMakeLists::add_executable()
{
    return "add_executable(${PROJECT_NAME} ${CPPS} ${HPPS})\n";
}

std::string CMakeLists::link_google_benchmark(bool is_enabled)
{
    return is_enabled == true ? "target_link_libraries(${PROJECT_NAME} PUBLIC benchmark benchmark_main pthread)\n" : "";
}


void CMakeLists::save_file(const std::string &path, const std::string &cmake_lists)
{
    const std::string file_name = "CMakeLists.txt";

    fs::path dir(path);
    if(path.empty() || !fs::is_directory(path))
        dir = fs::current_path();

    std::ofstream out(dir / file_name);

    out << cmake_lists;

    return ;
}
