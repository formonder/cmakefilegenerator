QT       += core gui widgets

QMAKE_CXXFLAGS += -std=c++17 -lstdc++fs

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    cmakeattributes.cpp \
    cmakelists.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    cmakeattributes.h \
    cmakelists.h \
    mainwindow.h

FORMS += \
    mainwindow.ui
