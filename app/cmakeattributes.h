#ifndef CMAKEATTRIBUTES_H
#define CMAKEATTRIBUTES_H

#include <string>
#include <vector>

enum class Sanitizers
{
    address, integer_overflow, memory, undefined
};

class CMakeAttributes
{
public:
    CMakeAttributes();

    const std::string& project_name() const;
    void project_name(const std::string& name);

    const std::string& cmake_version() const;
    void cmake_version(const std::string& version);

    const std::vector<std::string>& compiler_flags() const;
    void compiler_flags(const std::string& flags);

    const std::vector<std::string>& masks_sources() const;
    void masks_sources(const std::string& masks);

    const std::vector<std::string>& masks_headers() const;
    void masks_headers(const std::string& masks);

    const std::vector<std::string>& sanitizers() const;
    void sanitizers(const std::vector<Sanitizers>& masks);

    bool benchmark() const;
    void benchmark(bool is_enabled);

private:
    std::string              cmake_version_;
    std::string              project_name_;
    std::vector<std::string> compiler_flags_;
    std::vector<std::string> masks_sources_;
    std::vector<std::string> masks_headers_;
    std::vector<std::string> sanitizers_;
    bool                     benchmark_;
};

#endif // CMAKEATTRIBUTES_H
