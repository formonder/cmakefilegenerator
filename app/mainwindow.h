#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_cmake_button_clear_clicked();
    void on_cmake_button_generate_clicked();
    void on_cmake_button_path_clicked();
    void on_cmake_button_open_generate_clicked();
    void on_cmake_button_delete_generate_clicked();

private:
    void clear();
private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
