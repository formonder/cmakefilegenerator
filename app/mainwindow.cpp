#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "cmakeattributes.h"
#include "cmakelists.h"

#include <QDir>
#include <QFileDialog>
#include <QString>
#include <QDesktopServices>
#include <QMessageBox>

#include <filesystem>

namespace fs = std::filesystem;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->cmake_save_file_path->setText(QDir::homePath());
}

MainWindow::~MainWindow() { }

void MainWindow::on_cmake_button_clear_clicked()
{
    clear();
}

void createCMakeListsTxt(const std::string& path, const CMakeAttributes& attributes)
{
    const std::string cmake_lists_txt = CMakeLists::generateCMakeListsFile(attributes);
    CMakeLists::save_file(path, cmake_lists_txt);
}

std::vector<Sanitizers> selected_sanitizers(Ui::MainWindow *ui)
{
    std::vector<Sanitizers> sanitizers;

    if(ui->cmake_check_sanitizer_address->isChecked())
        sanitizers.push_back(Sanitizers::address);
    if(ui->cmake_check_sanitizer_integer_overflow->isChecked())
        sanitizers.push_back(Sanitizers::integer_overflow);
    if(ui->cmake_check_sanitizer_memory_leak->isChecked())
        sanitizers.push_back(Sanitizers::memory);
    if(ui->cmake_check_sanitizer_undefined->isChecked())
        sanitizers.push_back(Sanitizers::undefined);

    return sanitizers;
}

void MainWindow::on_cmake_button_generate_clicked()
{
    CMakeAttributes cmake_attributes;
    cmake_attributes.cmake_version(ui->cmake_version->text().toStdString());
    cmake_attributes.project_name(ui->cmake_project_name->text().toStdString());
    cmake_attributes.sanitizers(selected_sanitizers(ui));
    cmake_attributes.masks_headers(ui->cmake_header_extensions->text().toStdString());
    cmake_attributes.masks_sources(ui->cmake_source_extensions->text().toStdString());
    cmake_attributes.compiler_flags(ui->cmake_edit_cxx_flags->text().toStdString());
    cmake_attributes.benchmark(ui->cmake_check_google_benchmark->isChecked());

    createCMakeListsTxt(ui->cmake_save_file_path->text().toStdString(), cmake_attributes);
}

void MainWindow::on_cmake_button_path_clicked()
{
    ui->cmake_save_file_path->setText(QFileDialog::getExistingDirectory(this));
}

void MainWindow::clear()
{
    ui->cmake_version->clear();
    ui->cmake_project_name->clear();
    ui->cmake_edit_cxx_flags->clear();
    ui->cmake_header_extensions->clear();
    ui->cmake_source_extensions->clear();
    ui->cmake_check_sanitizer_address->setCheckState(Qt::Unchecked);
    ui->cmake_check_sanitizer_undefined->setCheckState(Qt::Unchecked);
    ui->cmake_check_sanitizer_memory_leak->setCheckState(Qt::Unchecked);
    ui->cmake_check_sanitizer_integer_overflow->setCheckState(Qt::Unchecked);
    ui->cmake_save_file_path->clear();
}

fs::path path_to_CMakefile(const std::string& directory)
{
    fs::path path(directory);
    path /= "CMakeLists.txt";
    return path;
}

void MainWindow::on_cmake_button_open_generate_clicked()
{
    const fs::path path = path_to_CMakefile(ui->cmake_save_file_path->text().toStdString());

    if(fs::is_regular_file(path))
    {
        QDesktopServices::openUrl(QString::fromStdString(path));
    }
    else
    {
        QMessageBox::warning(this, "Не удалось открыть файл", "Файл не найден!");
    }
}

void MainWindow::on_cmake_button_delete_generate_clicked()
{
    const fs::path path = path_to_CMakefile(ui->cmake_save_file_path->text().toStdString());

    if(fs::is_regular_file(path))
    {
        fs::remove(path);
        QMessageBox::information(this, "Успех", "Файл успешно удален.");
    }
    else
    {
        QMessageBox::warning(this, "Не удалось удалить файл", "Файл не найден!");
    }
}
